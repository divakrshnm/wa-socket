import { WAConnection, MessageType } from "@adiwajshing/baileys";
import * as fs from "fs";
import io from "socket.io-client";
import dotenv from "dotenv";
import { HOST } from "./config.js";

dotenv.config();

const socket = io.connect(HOST);

async function connectToWhatsApp() {
    const conn = new WAConnection();

    try {
        if (fs.existsSync("./auth_info.json")) {
            conn.loadAuthInfo("./auth_info.json");
        } else {
            conn.on("credentials-updated", () => {
                const authInfo = conn.base64EncodedAuthInfo();
                fs.writeFileSync(
                    "./auth_info.json",
                    JSON.stringify(authInfo, null, "\t")
                );
            });
        }
    } catch (err) {
        console.error(err);
    }

    await conn.connect();

    socket.on("wa-message", async (data, ackCallback) => {
        const { to, message } = data;
        try {
            await conn.sendMessage(
                to + "@s.whatsapp.net",
                message,
                MessageType.text,
                { detectLinks: true }
            );
            ackCallback("success");
            console.log(to + " success");
        } catch (error) {
            ackCallback("error");
            console.log(to + " error");
        }
    });
}

connectToWhatsApp().catch((err) => console.log("unexpected error: " + err));
